const {Given, When, Then} = require('cucumber');
const {Builder, By, until} = require('selenium-webdriver');
require('chromedriver');

Given('I am on the {string} page', async function (string){
    this.driver = new Builder()
    .forBrowser('chrome')
    .build();

    await this.driver.get(`https://the-internet.herokuapp.com/${string}`);
});

When('I login with {string} and {string}',function (string, string2){
    return 'pending';
});

Then('I should see a message saying {string}',function (string){
    return 'pending';
});